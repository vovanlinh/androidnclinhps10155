package com.example.vovanlinhps10155ass;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

public class NewsWed extends AppCompatActivity {
WebView wb;
Button btnback,btnsharelink;
//Toolbar toolbar;
    ShareDialog shareDialog;
    ShareLinkContent shareLinkContent;
     String link;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_wed);
        anhxa();
      //         setSupportActionBar(toolbar);

               //getlink

        Intent intent=getIntent();

         link=intent.getStringExtra("linktintuc");

        shareDialog=new ShareDialog(NewsWed.this);



        wb.loadUrl(link);
        wb.setWebViewClient(new WebViewClient());
        setonClick();

    }

    private void setonClick() {
        btnsharelink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if  (ShareDialog.canShow(ShareLinkContent.class)){
                    shareLinkContent=new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(link))
                            .build();
                }
                shareDialog.show(shareLinkContent);
            }
        });
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void anhxa() {
        wb=findViewById(R.id.wedviewnews);
        btnback=findViewById(R.id.btnwedback);
     //   toolbar = findViewById(R.id.toolbarnewswed);
btnsharelink=findViewById(R.id.btnsharelink);
    }
}
