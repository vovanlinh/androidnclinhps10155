package com.example.vovanlinhps10155ass;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vovanlinhps10155ass.Adapter.MonHocAdaptercv;
import com.example.vovanlinhps10155ass.Adapter.MonHocDKAdaptercv;
import com.example.vovanlinhps10155ass.Dao.DANGKYMonHocDAO;
import com.example.vovanlinhps10155ass.Dao.MonHocDAO;
import com.example.vovanlinhps10155ass.Model.DANGKYMonHoc;

import java.util.ArrayList;


public class CourseFragmentxem extends Fragment {
    RecyclerView lv;

    public static MonHocDKAdaptercv adapter;
    static DANGKYMonHocDAO dao;
    ArrayList<DANGKYMonHoc> list;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_xemcourse,container,false);


    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv = view.findViewById(R.id.rcv_monhocdadk);
        list = new ArrayList<>();
        dao = new DANGKYMonHocDAO(getContext());
        list = dao.LayTatCaMonHocDK();
        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
        adapter = new MonHocDKAdaptercv(getContext(), list);

        lv.setAdapter(adapter);
    }





}
