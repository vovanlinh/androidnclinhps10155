package com.example.vovanlinhps10155ass;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.vovanlinhps10155ass.Dao.MonHocDAO;
import com.example.vovanlinhps10155ass.Model.MonHoc;
import com.example.vovanlinhps10155ass.Service.CourseService;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class CourseFragmentthem extends Fragment {
EditText edttenmon,edtngayhoc,edtngaythi;
TextView asd;
Button btnpickdate,btnthem,btnhuy,btnpickdatethi;
TextInputLayout inputtenmon,inputngayhoc,inputngaythi;
    MonHocDAO dao;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_themcourse,container,false);


    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dao = new MonHocDAO(getContext());
        edttenmon=view.findViewById(R.id.edttenmon);
        edtngayhoc=view.findViewById(R.id.edtngayhoc);
        btnpickdate=view.findViewById(R.id.btnpicdate);
        btnhuy=view.findViewById(R.id.btnhuynd);
        inputtenmon=view.findViewById(R.id.inputtenmon);
        inputngayhoc=view.findViewById(R.id.inputngayhoc);
        inputngaythi=view.findViewById(R.id.inputngaythi);
        btnpickdatethi=view.findViewById(R.id.btnpicdatethi);
        edtngaythi=view.findViewById(R.id.edtngaythic);
        btnpickdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar=Calendar.getInstance();
                int ngay=calendar.get(Calendar.DATE);
                int thang=calendar.get(Calendar.MONTH);
                int nam=calendar.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendar.set(year,month,dayOfMonth);
                        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
                        edtngayhoc.setText(simpleDateFormat.format(calendar.getTime()));
                    }
                },nam,thang,ngay);
                datePickerDialog.show();
            }
        });
        btnpickdatethi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendarthi=Calendar.getInstance();
                int ngaythi=calendarthi.get(Calendar.DATE);
                int thangthi=calendarthi.get(Calendar.MONTH);
                int namthi=calendarthi.get(Calendar.YEAR);
                DatePickerDialog datePickerDialog=new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        calendarthi.set(year,month,dayOfMonth);
                        SimpleDateFormat simpleDateFormatthi=new SimpleDateFormat("dd/MM/yyyy");
                        edtngaythi.setText(simpleDateFormatthi.format(calendarthi.getTime()));
                    }
                },namthi,thangthi,ngaythi);
                datePickerDialog.show();
            }
        });
        btnthem=view.findViewById(R.id.btnthemnd);
        btnthem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    format.parse(edtngayhoc.getText().toString());
                } catch (ParseException e) {
                    inputngayhoc.setError("Dinh dang khong hop le");
                    e.printStackTrace();
                    return;
                }
                try {
                    format.parse(edtngaythi.getText().toString());
                } catch (ParseException e) {
                    inputngaythi.setError("Dinh dang khong hop le");
                    e.printStackTrace();
                    return;
                }
                Boolean checkmon=dao.checkmonhoc(edttenmon.getText().toString());
if (edttenmon.getText().toString().isEmpty()){
    inputtenmon.setError("Ten mon khong duoc de trong");
    return;
}
else if (edtngayhoc.getText().toString().isEmpty()){
            inputngayhoc.setError("Ngay hoc khong duoc de trong");
            return;
                }
               else if (checkmon==false){
                    inputtenmon.setError("Ten mon da ton tai");
                    return;
                }
                else {
    Intent intent=new Intent(getActivity(), CourseService.class);
    intent.putExtra("tenmon",edttenmon.getText().toString());
    intent.putExtra("ngayhoc",edtngayhoc.getText().toString());
    intent.putExtra("ngaythi",edtngaythi.getText().toString());
    getActivity().startService(intent);

    edttenmon.setText("");
    edtngayhoc.setText("");
    edtngaythi.setText("");
                    edttenmon.setText("");
                    edtngayhoc.setText("");
    edtngaythi.setText("");
                }



            }
        });
        btnhuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edttenmon.setText("");
                edtngayhoc.setText("");
                edtngaythi.setText("");
            }
        });
    }





}
