package com.example.vovanlinhps10155ass.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vovanlinhps10155ass.CourseFragmentxem;
import com.example.vovanlinhps10155ass.Dao.DANGKYMonHocDAO;
import com.example.vovanlinhps10155ass.Dao.MonHocDAO;
import com.example.vovanlinhps10155ass.MainActivity;
import com.example.vovanlinhps10155ass.Model.DANGKYMonHoc;
import com.example.vovanlinhps10155ass.Model.MonHoc;
import com.example.vovanlinhps10155ass.R;
import com.example.vovanlinhps10155ass.Service.CourseServiceDK;

import java.io.IOException;
import java.util.ArrayList;

public class MonHocAdaptercv extends RecyclerView.Adapter<MonHocAdaptercv.ViewHolder> {
    Context context;
    ArrayList<MonHoc> dulieund;
    DANGKYMonHocDAO dao;
    MonHocDAO monHocDAO;
    ArrayList<DANGKYMonHoc> list;
    public MonHocAdaptercv(Context context, ArrayList<MonHoc> dulieund) {
        this.context = context;
        this.dulieund = dulieund;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView ngayhoc,tenmon;
        public ImageView popupxoasua;
        public ViewHolder(View itemview){
            super(itemview);

            ngayhoc=itemview.findViewById(R.id.cvtxt_ngayhoc);
            popupxoasua=itemview.findViewById(R.id.popupxoasua);
            tenmon=itemview.findViewById(R.id.cvtxt_tenmon);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View v = LayoutInflater.from(viewGroup.getContext())
        .inflate(R.layout.cardview_monhoc,viewGroup,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
      final MonHoc nd=dulieund.get(i);
      if (nd!=null){

          viewHolder.ngayhoc.setText(nd.getNgayHoc());
          viewHolder.tenmon.setText(nd.getTenMon());
      }
        viewHolder.popupxoasua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup=new PopupMenu(v.getContext(),v);// co the de view thay cho text view van dc
                MenuInflater inflater=popup.getMenuInflater();
                inflater.inflate(R.menu.poup_menu_kt,popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.poupXoakt:
                                AlertDialog.Builder builder=new AlertDialog.Builder(context,android.R.style.Theme_DeviceDefault_Light_Dialog);
                                builder.setTitle("Ban co chac muon xoa");
                                builder.setMessage("Hay lua chon ben duoi de xac nhan");
                                builder.setIcon(android.R.drawable.ic_dialog_alert);
                                builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                        nd.getMaMon();
                                        nd.getTenMon();
                                        nd.getNgayHoc();
                                        nd.getNgayThi();

                                        monHocDAO=new MonHocDAO(context);
                                        if (monHocDAO.XoaLoai(nd)<=-1){
                                            Toast.makeText(context, "Xoa khong thanh cong", Toast.LENGTH_SHORT).show();
                                        } Toast.makeText(context, "Xoa thanh cong", Toast.LENGTH_SHORT).show();
                                        dulieund.remove(nd); // cach nay hay hon it ton du lieu
                                        MonHocAdaptercv.this.notifyDataSetChanged();
                                    }
                                });
                                builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int w) {

                                    }
                                });
                                builder.show();
                                break;
                            case R.id.poupSuakt:
                                break;
                            case R.id.poupDangky:
                            //    list = new ArrayList<>();
                                dao = new DANGKYMonHocDAO(context);
                              //  list = dao.LayTatCaMonHocDK();
                                String tenmonhoc=nd.getTenMon();
                                String tenmonhocdk;

// dang loi~

                                Boolean checkmon=dao.checkmonhoc(tenmonhoc);

if (checkmon==false){
    Toast.makeText(context, "Ban da dang ky mon hoc nay roi", Toast.LENGTH_SHORT).show();
    break;
}else {
    Intent intent=new Intent(context, CourseServiceDK.class);
    intent.putExtra("tenmon",nd.getTenMon());
    intent.putExtra("ngayhoc",nd.getNgayHoc());
    intent.putExtra("ngaythi",nd.getNgayThi());
    context.startService(intent);
}

                                break;

                        }
                        return false;
                    }
                });
                popup.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return dulieund.size();
    }


}

