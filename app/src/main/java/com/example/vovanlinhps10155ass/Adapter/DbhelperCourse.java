package com.example.vovanlinhps10155ass.Adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbhelperCourse extends SQLiteOpenHelper {
    public final static String DBSNAME = "HOTROHOCTAP";
    public final static int DBVERSION = 1;
    public DbhelperCourse(Context context){super(context,DBSNAME,null,DBVERSION);}

    @Override
    public void onCreate(SQLiteDatabase db) {
        // tao bang loai thu chi
        String sql="CREATE TABLE HOCTAP("
                +"MAMON INTEGER PRIMARY KEY AUTOINCREMENT,"
                +"TENMON TEXT,"
                + "NGAYHOC TEXT,"
                +"NGAYTHI TEXT)";
        db.execSQL(sql);
        String sqldk="CREATE TABLE DANGKYHOCTAP("
                +"MAMONDK INTEGER PRIMARY KEY AUTOINCREMENT,"
                +"TENMONDK TEXT,"
                + "NGAYHOCDK TEXT,"
                +"NGAYTHIDK TEXT)";
        db.execSQL(sqldk);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sqlloaithuchi="DROP TABLE HOCTAP";
        db.execSQL(sqlloaithuchi);
        String sqlloaithuchiDK="DROP TABLE DANGKYHOCTAP";
        db.execSQL(sqlloaithuchiDK);
        onCreate(db);
    }
}
