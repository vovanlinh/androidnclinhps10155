package com.example.vovanlinhps10155ass;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class CourseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.coursefragment,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container_course,new CourseFragmentthem());
        transaction.commit();
        BottomNavigationView bottomnav=view.findViewById(R.id.bottom_navi_course);
        bottomnav.setOnNavigationItemSelectedListener(Navlister);

    }
    BottomNavigationView.OnNavigationItemSelectedListener Navlister=new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment=null;
            switch (menuItem.getItemId()){
                case R.id.nav_themkhoahoc:
                    selectedFragment=new CourseFragmentthem();
                    break;
                case R.id.nav_dangkykhoahoc:
                    selectedFragment=new CourseFragmentdangky();
                    break;
                case R.id.nav_xemkhoahoc:
                    selectedFragment=new CourseFragmentxem();
                    break;
            }
            FragmentTransaction transaction=getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container_course,selectedFragment);
            transaction.commit();
            return true;
        }
    };
}
