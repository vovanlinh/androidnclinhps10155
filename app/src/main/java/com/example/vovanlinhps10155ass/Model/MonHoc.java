package com.example.vovanlinhps10155ass.Model;

public class MonHoc {
    int maMon;
    String tenMon;
    String ngayHoc;
    String ngayThi;
    public MonHoc() {
    }

    public int getMaMon() {
        return maMon;
    }

    public void setMaMon(int maMon) {
        this.maMon = maMon;
    }

    public String getTenMon() {
        return tenMon;
    }

    public void setTenMon(String tenMon) {
        this.tenMon = tenMon;
    }

    public String getNgayHoc() {
        return ngayHoc;
    }

    public void setNgayHoc(String ngayHoc) {
        this.ngayHoc = ngayHoc;
    }

    public String getNgayThi() {
        return ngayThi;
    }

    public void setNgayThi(String ngayThi) {
        this.ngayThi = ngayThi;
    }

    public MonHoc(int maMon, String tenMon, String ngayHoc, String ngayThi) {
        this.maMon = maMon;
        this.tenMon = tenMon;
        this.ngayHoc = ngayHoc;
        this.ngayThi = ngayThi;
    }
}
