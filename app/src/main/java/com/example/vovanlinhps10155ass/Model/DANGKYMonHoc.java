package com.example.vovanlinhps10155ass.Model;

public class DANGKYMonHoc {
    int maMonDK;
    String tenMonDK;
    String ngayHocDK;
    String ngayThiDK;

    public int getMaMonDK() {
        return maMonDK;
    }

    public void setMaMonDK(int maMonDK) {
        this.maMonDK = maMonDK;
    }

    public String getTenMonDK() {
        return tenMonDK;
    }

    public void setTenMonDK(String tenMonDK) {
        this.tenMonDK = tenMonDK;
    }

    public String getNgayHocDK() {
        return ngayHocDK;
    }

    public void setNgayHocDK(String ngayHocDK) {
        this.ngayHocDK = ngayHocDK;
    }

    public String getNgayThiDK() {
        return ngayThiDK;
    }

    public void setNgayThiDK(String ngayThiDK) {
        this.ngayThiDK = ngayThiDK;
    }

    public DANGKYMonHoc(int maMonDK, String tenMonDK, String ngayHocDK, String ngayThiDK) {
        this.maMonDK = maMonDK;
        this.tenMonDK = tenMonDK;
        this.ngayHocDK = ngayHocDK;
        this.ngayThiDK = ngayThiDK;
    }

    public DANGKYMonHoc() {
    }
}
