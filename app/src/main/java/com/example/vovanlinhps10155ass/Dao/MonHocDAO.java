package com.example.vovanlinhps10155ass.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vovanlinhps10155ass.Adapter.DbhelperCourse;
import com.example.vovanlinhps10155ass.Model.MonHoc;

import java.util.ArrayList;

public class MonHocDAO {
    SQLiteDatabase db;
    SQLiteDatabase database;
  //  DbHelper helper;// them
    public MonHocDAO(Context context){
        DbhelperCourse dbHelper=new DbhelperCourse(context);
       db=dbHelper.getWritableDatabase();
       database=dbHelper.getReadableDatabase();
       // helper=new DbHelper(context);//them
    }


    public ArrayList<MonHoc> LayTatCaMonHoc(){

        ArrayList<MonHoc> danhsachloai=new ArrayList<>();
        String sql ="SELECT * FROM HOCTAP";
        Cursor c=db.rawQuery(sql,null);
        while (c.moveToNext()){
            MonHoc L=new MonHoc();
            L.setMaMon(Integer.parseInt(c.getString(c.getColumnIndex("MAMON"))));
            L.setTenMon(c.getString(c.getColumnIndex("TENMON")));
            L.setNgayHoc(c.getString(c.getColumnIndex("NGAYHOC")));
            L.setNgayThi(c.getString(c.getColumnIndex("NGAYTHI")));
            danhsachloai.add(L);
        }
        return danhsachloai;
    }




    /**
     *
     * @Result: ArrayList<loaithuchi>
     */

    public long ThemMon(MonHoc l) {
     //   SQLiteDatabase db=helper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("TENMON", l.getTenMon());
        values.put("NGAYHOC",l.getNgayHoc());
        values.put("NGAYTHI",l.getNgayThi());
        return db.insert("HOCTAP", null, values);
    }

    public int XoaLoai(MonHoc l){
       // SQLiteDatabase db=helper.getReadableDatabase();
        return db.delete("HOCTAP","MAMON=?",new String[]{l.getMaMon()+""});
    }

    public int SuaLoai(MonHoc l){
        //SQLiteDatabase db=helper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("TENMON", l.getTenMon());
        //values.put("TRANGTHAI",l.getTrangthai());
        values.put("NGAYHOC",l.getNgayHoc());
        values.put("NGAYTHI",l.getNgayThi());
        return db.update("HOCTAP",values,"MAMON=?",new String[]{l.getMaMon()+""});

    }
    public Boolean checkmonhoc(String tenmon){
        Cursor cursor =database.rawQuery("Select * from HOCTAP where TENMON=?",new String[]{tenmon});
        if (cursor.getCount()>0)return false;
        else return true;
    }

}
