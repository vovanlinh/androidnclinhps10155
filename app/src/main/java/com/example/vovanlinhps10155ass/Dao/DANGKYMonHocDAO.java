package com.example.vovanlinhps10155ass.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vovanlinhps10155ass.Adapter.DbhelperCourse;
import com.example.vovanlinhps10155ass.Model.DANGKYMonHoc;
import com.example.vovanlinhps10155ass.Model.MonHoc;

import java.util.ArrayList;

public class DANGKYMonHocDAO {
    SQLiteDatabase db;
    SQLiteDatabase database;
  //  DbHelper helper;// them
    public DANGKYMonHocDAO(Context context){
        DbhelperCourse dbHelper=new DbhelperCourse(context);
       db=dbHelper.getWritableDatabase();
       // helper=new DbHelper(context);//them
        database=dbHelper.getReadableDatabase();
    }


    public ArrayList<DANGKYMonHoc> LayTatCaMonHocDK(){

        ArrayList<DANGKYMonHoc> danhsachloai=new ArrayList<>();
        String sql ="SELECT * FROM DANGKYHOCTAP";
        Cursor c=db.rawQuery(sql,null);
        while (c.moveToNext()){
            DANGKYMonHoc L=new DANGKYMonHoc();
            L.setMaMonDK(Integer.parseInt(c.getString(c.getColumnIndex("MAMONDK"))));
            L.setTenMonDK(c.getString(c.getColumnIndex("TENMONDK")));
            L.setNgayHocDK(c.getString(c.getColumnIndex("NGAYHOCDK")));
            L.setNgayThiDK(c.getString(c.getColumnIndex("NGAYTHIDK")));
            danhsachloai.add(L);
        }
        return danhsachloai;
    }




    /**
     *
     * @Result: ArrayList<loaithuchi>
     */

    public long ThemMonDK(DANGKYMonHoc l) {
     //   SQLiteDatabase db=helper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("TENMONDK", l.getTenMonDK());
        values.put("NGAYHOCDK",l.getNgayHocDK());
        values.put("NGAYTHIDK",l.getNgayThiDK());
        return db.insert("DANGKYHOCTAP", null, values);
    }

    public int XoaLoaiDK(DANGKYMonHoc l){
       // SQLiteDatabase db=helper.getReadableDatabase();
        return db.delete("DANGKYHOCTAP","MAMONDK=?",new String[]{l.getMaMonDK()+""});
    }

    public int SuaLoaiDK(DANGKYMonHoc l){
        //SQLiteDatabase db=helper.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("TENMONDK", l.getTenMonDK());
        //values.put("TRANGTHAI",l.getTrangthai());
        values.put("NGAYHOCDK",l.getNgayHocDK());
        return db.update("DANGKYHOCTAP",values,"MAMONDK=?",new String[]{l.getMaMonDK()+""});

    }
    public Boolean checkmonhoc(String tenmon){
       Cursor cursor =database.rawQuery("Select * from DANGKYHOCTAP where TENMONDK=?",new String[]{tenmon});
       if (cursor.getCount()>0)return false;
       else return true;
    }

}
