package com.example.vovanlinhps10155ass;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vovanlinhps10155ass.Adapter.MonHocAdaptercv;
import com.example.vovanlinhps10155ass.Dao.MonHocDAO;
import com.example.vovanlinhps10155ass.Model.MonHoc;

import java.util.ArrayList;


public class CourseFragmentdangky extends Fragment {
    RecyclerView lv;

    public static MonHocAdaptercv adapter;
    static MonHocDAO dao;
    ArrayList<MonHoc> list;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dangkycourse,container,false);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv = view.findViewById(R.id.rcv_dkmonhoc);
        list = new ArrayList<>();
        dao = new MonHocDAO(getContext());
        list = dao.LayTatCaMonHoc();
        LinearLayoutManager mlinerlayoutmanager=new LinearLayoutManager(getContext());
        lv.setLayoutManager(mlinerlayoutmanager);
        adapter = new MonHocAdaptercv(getContext(), list);

        lv.setAdapter(adapter);
    }





}
