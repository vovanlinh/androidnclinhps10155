package com.example.vovanlinhps10155ass.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.example.vovanlinhps10155ass.Dao.DANGKYMonHocDAO;
import com.example.vovanlinhps10155ass.Dao.MonHocDAO;
import com.example.vovanlinhps10155ass.Model.DANGKYMonHoc;
import com.example.vovanlinhps10155ass.Model.MonHoc;

public class CourseServiceDK extends Service {
    DANGKYMonHocDAO dao;
    DANGKYMonHoc ltc;
    public CourseServiceDK() {
    }

    @Override
    public void onCreate() {


        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


         ltc = new DANGKYMonHoc();
        dao = new DANGKYMonHocDAO(this);
        String ten=intent.getStringExtra("tenmon");
        String hoc=intent.getStringExtra("ngayhoc");
        String thi=intent.getStringExtra("ngaythi");
        DANGKYMonHoc ltc = new DANGKYMonHoc();
        ltc.setTenMonDK(ten);
        ltc.setNgayHocDK(hoc);
        ltc.setNgayThiDK(thi);
        if (dao.ThemMonDK(ltc) == -1) {
            Toast.makeText(this, "Them khong duoc", Toast.LENGTH_SHORT).show();

        }
        Toast.makeText(this, "Them thanh cong", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
      return null;
    }
}
